﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour {
    public float mass = 1.0f;
    private float speed = 0.0f;
    private Vector2 dir = new Vector2();
    private float targetmass = 0.0f;
    private float g = 0.0f;
    private float gspeed = 0.0f;
    public float grange = 8.0f;
    private Vector2 gv = new Vector2();
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.Translate(speed * dir * Time.deltaTime);
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("1");
        targetmass = collision.transform.localScale.magnitude;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        dir = (collision.transform.position - transform.position);
        g = (mass)/(dir.magnitude * dir.magnitude);
        if (dir.magnitude > grange)
        {
            g = 0;
            gspeed = gspeed - 0.01f * Time.deltaTime;
            if (gspeed < 0) gspeed = 0.0f;
        }
        gspeed = gspeed + g;

        collision.transform.Translate( - dir.normalized * gspeed * Time.deltaTime);
        print(dir.magnitude);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
    }
}
