﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circle : MonoBehaviour {
    public int vertex = 50;
    public float width = 0.1f;
    public float radius;
    public LineRenderer line;

	// Use this for initialization
	void Start () {
        float d = (2f * Mathf.PI) / vertex;
        float t = 0.0f;
        line.positionCount = vertex;
        for(int i = 0; i < line.positionCount; i++)
        {
            Vector3 pos = new Vector3(radius * Mathf.Cos(t), radius * Mathf.Sin(t), 0.0f);
            line.SetPosition(i, pos);
            t = t + d;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
