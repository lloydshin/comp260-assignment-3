﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleControl : MonoBehaviour {
    public ParticleSystem back;
    public ParticleSystem backboost;
    public ParticleSystem front;
    public ParticleSystem frontboost;
    public ParticleSystem turnleft1;
    public ParticleSystem turnleft2;
    public ParticleSystem turnright1;
    public ParticleSystem turnright2;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.W))
        {
            back.Play();
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            back.Stop();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
           
            front.Play();
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            front.Stop();
        }

        if (Input.GetKeyDown(KeyCode.Space) && Input.GetKey(KeyCode.W))
        {
            backboost.Play();
        }

        if (Input.GetKeyDown(KeyCode.Space) && Input.GetKey(KeyCode.S))
        {
            frontboost.Play();
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            backboost.Stop();
            frontboost.Stop();
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            turnleft1.Play();
            turnleft2.Play();
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            turnleft1.Stop();
            turnleft2.Stop();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            turnright1.Play();
            turnright2.Play();
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            turnright1.Stop();
            turnright2.Stop();
        }
    }
}
