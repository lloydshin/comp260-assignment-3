﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
    
    public float maxTurnSpeed = 30.0f;
    public float acceleration = 1.0f;
    public float boost = 5.0f;
    public float turnAcc = 2.0f;
    private float turnSpeed = 0.0f;
    private float speed = 0.0f;
    private float temp = 0.0f;
    public float border = 80f;
    private float gspeed = 0.0f;
    //g
    public float mass = 1.0f;
    private float g = 0.0f;
    private Vector2 gdir = new Vector2();
    private Vector2 ngdir = new Vector2();
    private bool geffect = false;
    private float targetmass = 0.0f;
    // Use this for initialization
    void Start () {
        temp = acceleration;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        //border
        if(transform.position.x >= border)
        {
            Vector3 t = new Vector3(-2 * border, 0, 0);
            transform.position += t;
        }

        if (transform.position.x <= -border)
        {
            Vector3 t = new Vector3(2 * border, 0, 0);
            transform.position += t;
        }

        if (transform.position.y <= -border)
        {
            Vector3 t = new Vector3(0, 2 * border, 0);
            transform.position += t;
        }

        if (transform.position.y >= border)
        {
            Vector3 t = new Vector3(0, -2 * border, 0);
            transform.position += t;
        }

        float forwards = Input.GetAxis("Vertical");
        if (Input.GetKey("space"))
        {
            acceleration = boost;
        }
        else
        {
            acceleration = temp;
        }
       
        //accelerate
        if (forwards > 0)
        {
            speed = speed + acceleration * Time.deltaTime;
        }
        else if(forwards < 0)
        {
            speed = speed - acceleration * Time.deltaTime;
        }

        transform.Translate(Vector2.up * speed * Time.deltaTime);
        gspeed = gspeed + g * Time.deltaTime;
        transform.Translate(ngdir.normalized * gspeed * Time.deltaTime);

        float turn = Input.GetAxis("Horizontal");
        turnSpeed = turnSpeed - turn * turnAcc * Time.deltaTime;
        ngdir = Vrotate(gdir, transform.eulerAngles.magnitude);
        transform.Rotate(0, 0, turnSpeed);
        if(geffect == false && gspeed != 0)
        {
            speed = (Vector2.up * speed + ngdir.normalized * gspeed).magnitude;
            gspeed = 0;
        }
        print(gspeed);
        //g

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("1");
        geffect = true;
        targetmass = collision.transform.localScale.z;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        gdir = (collision.transform.position - transform.position);
        
        g = (targetmass * mass) / (gdir.magnitude * gdir.magnitude);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        geffect = false;
    }

    private Vector2 Vrotate(Vector2 v, float angle)
    {
        float rad = angle * Mathf.Deg2Rad;
        float x = v.x;
        float y = v.y;
        float newX = x * Mathf.Cos(rad) + y * Mathf.Sin(rad);
        float newY = -x * Mathf.Sin(rad) + y * Mathf.Cos(rad);
        Vector2 result = new Vector2(newX, newY);
        return result;
    }
}
